import numpy as np
import cv2
import imutils






def draw_detections(img, rects, thickness = 1):
    for x, y, w, h in rects:
        # the HOG detector returns slightly larger rectangles than the real objects.
        # so we slightly shrink the rectangles to get a nicer output.

        pad_w, pad_h = int(0.15*w), int(0.05*h)
        cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0, 255, 0), thickness)

face_cascade=cv2.CascadeClassifier('/Users/kere-mac/anaconda3/lib/python3.6/site-packages/cv2/data/haarcascade_fullbody.xml')

if __name__ == '__main__':

    cap=cv2.VideoCapture('Media/Video/opticalFlow.mp4')
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    while True:
        _,frame=cap.read()
        frame = imutils.resize(frame, width=min(800, frame.shape[1]))
        #find human region
        #compare if diff from previous one
        #if diff then update previous one
        #if not then just track detected region using mean shift or cam shift
        faces,w=hog.detectMultiScale(frame, winStride=(8,8), padding=(32,32), scale=1.05)

        # faces = face_cascade.detectMultiScale(cv2.cvtColor(frame,cv2.COLOR_RGB2GRAY), 1.1, 1)
        draw_detections(frame,faces)
        cv2.imshow('feed',frame)
        ch = 0xFF & cv2.waitKey(1)
        if ch == 27:
            break
    cv2.destroyAllWindows()