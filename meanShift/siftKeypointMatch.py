import cv2
import numpy as np
imageQuery=cv2.imread('Media/Pictures/SinglePerson/SiftFrame1_1.png',cv2.COLOR_RGB2GRAY)
imageTest=cv2.imread('Media/Pictures/SinglePerson/SiftFrame1_2.png',cv2.COLOR_RGB2GRAY)
detector = cv2.xfeatures2d.SIFT_create()
keyQ, desQ = detector.detectAndCompute(imageQuery, None)
keyT,desT=detector.detectAndCompute(imageTest,None)

imageQueryDraw=cv2.drawKeypoints(imageQuery,keyQ,None)
imageTestDraw=cv2.drawKeypoints(imageTest,keyT,None)

FLANN_INDEX_KDTREE = 0

index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks = 5000)
flann = cv2.FlannBasedMatcher(index_params, search_params)
matches = flann.knnMatch(desQ,desT,k=2)

matchesMask = [[0,0] for i in range(len(matches))]

for i,(m,n) in enumerate(matches):
    if m.distance < 0.7*n.distance:
        matchesMask[i] = [1, 0]

draw_params = dict(matchColor = (0,255,0),
                   singlePointColor = (255,0,0),
                   matchesMask = matchesMask,
                   flags = 0)

img3 = cv2.drawMatchesKnn(imageQuery,keyQ,imageTest,keyT,matches,None,**draw_params)


# cv2.imshow('queryKeypointDraw',imageQueryDraw)
# cv2.imshow('testKeypointDraw',imageTestDraw)
cv2.imshow('result',img3)

cv2.waitKey(0)
