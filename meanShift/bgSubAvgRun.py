

import cv2
import sys
import numpy as np

def nothing(*arg):
    pass
# gizza window with a trackbar on it
cv2.namedWindow('bgmodel')
cv2.namedWindow('foregound')
cv2.namedWindow('input')
cv2.createTrackbar('Size of buffer', 'bgmodel', 110, 500, nothing)
cv2.createTrackbar('Difference threshold', 'bgmodel', 10, 200, nothing)

n = 0
# gizza videocapture object
cap = cv2.VideoCapture('Media/Video/QueueNew.mp4')

flag, img = cap.read()
movingaverage = np.float32(img)
while True:

    # read a frame from the video capture obj

    flag, img = cap.read()

    fbuffer = cv2.getTrackbarPos('Size of buffer', 'bgmodel')
    # let's deal with that pesky zero case before we divide by fbuffer
    if fbuffer == 0:
        fbuffer = 1
    alpha = float(1.0 / fbuffer)
    cv2.accumulateWeighted(img, movingaverage, alpha)

    # do the drawing stuff
    res = cv2.convertScaleAbs(movingaverage)
    # show the background model
    cv2.imshow('bgmodel', res)

    # resize the input just so i can have a smaller window and still show the input
    # on a little laptop screen
    tmp = cv2.resize(img, (0, 0), fx=0.5, fy=0.5)

    # take the absolute difference of the background and the input
    difference_img = cv2.absdiff(res, img)
    # make that greyscale

    grey_difference_img = cv2.cvtColor(difference_img, cv2.COLOR_BGR2GRAY)
    # threshold it to get a motion mask
    difference_thresh = cv2.getTrackbarPos('Difference threshold', 'bgmodel')
    ret, th1 = cv2.threshold(grey_difference_img, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    im2, contours, hierarchy = cv2.findContours(th1, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    cnts=[x for x in contours if cv2.contourArea(x)>1000 and cv2.contourArea(x)<2000]
    for i in range(len(cnts)):
        x, y, w, h = cv2.boundingRect(cnts[i])
        cv2.rectangle(tmp, (x, y), (x + w, y + h), (0, 255, 0), 2)


    cv2.imshow('foregound', th1)
    cv2.imshow('input', tmp)
    # uncommment the next few lines if you want to save the output
    # fn="out/bgmovingav_big"+str(n).rjust(4,'0')+".png"
    # cv2.imwrite(fn,th1);
    # fn="out/bgmovingav_bg_big"+str(n).rjust(4,'0')+".png"
    # cv2.imwrite(fn,res);
    n += 1

    # open cv window management/redraw stuff
    ch = cv2.waitKey(5)
    if ch == 27:
        break
cv2.destroyAllWindows()