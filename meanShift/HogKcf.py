import numpy as np
import cv2
import imutils
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import csv
import os.path
import collections
import operator

xyCord=[]


def draw_detections(frame,founds,w):
    for (x,y,w,h),weight in zip(founds,w):
        tmpList=[]

        tmpList.append(x+(w/2))
        tmpList.append(y+(h/2))
        xyCord.append(tmpList)
        # pad_w, pad_h = int(0.15*w), int(0.05*h)
        # font = cv2.FONT_HERSHEY_SIMPLEX
        # cv2.rectangle(frame, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0, 255, 0), 2)
        # cv2.putText(frame, '{}'.format(weight), (x, y), font, 0.8, (255, 0, 0), 2,cv2.LINE_AA)


def getEqualChunks(list,numOfChunks):
    L = len(list)
    assert 0 < numOfChunks <= L
    s = L // numOfChunks
    return [list[p:p + s] for p in range(0, L, s)]


def getIndOfCentre(kmeansObj):
    lablesList=kmeans.labels_
    counter=collections.Counter(lablesList)
    lable=max(counter.items(),key=operator.itemgetter(1))[0]
    return lable

def splitList(centre):
    if centre[0]<=400:
        return 0
    elif centre[0]<=800:
        return 1
    elif centre[0]<=1200:
        return 2
    else:
        return 3

def calBbox(boxCentre):
    width=380
    height=100
    x=boxCentre[0]-(width/2)
    y=boxCentre[1]-(height/2)
    return (x,y,width,height)


def getBoundingBoxes(centres):
    boundingBoxes=[]
    masks=list(map(splitList,centres))
    list0=np.array([c for (c,m) in zip(centres,masks) if m==0])
    list1 = np.array([c for (c, m) in zip(centres, masks) if m == 1])
    list2 = np.array([c for (c, m) in zip(centres, masks) if m == 2])
    list3 = np.array([c for (c, m) in zip(centres, masks) if m == 3])
    boundingBoxes.append(np.mean(list0,axis=0))
    boundingBoxes.append(np.mean(list1,axis=0))
    boundingBoxes.append(np.mean(list2,axis=0))
    boundingBoxes.append(np.mean(list3,axis=0))
    finalList=[]
    for boxCentre in boundingBoxes:
        width = 380
        height = 300
        x = boxCentre[0] - (width / 2)
        y = boxCentre[1] - (height / 2)
        finalList.append((int(x),int(y),width,height))

    return finalList

def getRois(boundingbox):
    (x, y, width, height)=boundingbox
    roi=image[int(y):int(y)+height,int(x):int(x)+width]
    hsv_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv_roi, np.array((0., 60., 32.)), np.array((180., 255., 255.)))
    roi_hist = cv2.calcHist([hsv_roi], [0], mask, [180], [0, 180])
    cv2.normalize(roi_hist, roi_hist, 0, 255, cv2.NORM_MINMAX)
    return roi_hist

def getTrackedWindows(val):
    track_window, roi_hist=val
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    dst = cv2.calcBackProject([hsv], [0], roi_hist, [0, 180], 1)
    # apply meanshift to get the new location
    ret, tracked_window = cv2.meanShift(dst, track_window, term_crit)
    return tracked_window


if __name__ == '__main__':
    cap = cv2.VideoCapture('Media/Video/QueueNew.mp4')
    if not os.path.isfile('points.csv'):
        totalFrame= int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        hog = cv2.HOGDescriptor()
        hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        frameNum=1
        while True:
            if (frameNum>400 or frameNum==totalFrame):
                break
            _, frame = cap.read()
            frameNum=frameNum+1
            print(frameNum)
            frame = imutils.resize(frame, width=min(1600, frame.shape[1]))
            found, w = hog.detectMultiScale(frame, winStride=(8, 8), padding=(32, 32), scale=1.05)
            if (found != ()):
                draw_detections(frame, found, w)
                ch = 0xFF & cv2.waitKey(1)
                if ch == 27:
                    break
        plt.scatter(*zip(*xyCord))
        plt.show()
        xyCord.sort(key=lambda x: x[0])
        with open('points.csv', 'w') as csvfile:
            fieldnames = ['x', 'y']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for val in xyCord:
                writer.writerow({'x': val[0], 'y': val[1]})

    del xyCord[:]
    xyCord = []
    with open('points.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            xyCord.append([row['x'], row['y']])

    chunks = getEqualChunks(xyCord, 8)
    centres = []
    for chunk in chunks:
        if(len(chunk)<=3):
            continue
        kmeans = KMeans(n_clusters=3, random_state=0).fit(chunk)
        interestCentIndx = getIndOfCentre(kmeans)
        centres.append(kmeans.cluster_centers_[interestCentIndx])
        plt.scatter(*zip(*kmeans.cluster_centers_))
    plt.show()
    boundingBoxes=getBoundingBoxes(centres)

    #meanshift from here
    # ok, image = cap.read()
    # roi_hists=list(map(getRois,boundingBoxes))

    tracker = cv2.MultiTracker_create()
    init_once = False
    term_crit = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
    while cap.isOpened():
        ok, image = cap.read()
        if not ok:
            print('no image to read')
            break

        if not init_once:
            ok = tracker.add(cv2.TrackerMIL_create(), image, boundingBoxes[0])
            ok = tracker.add(cv2.TrackerMIL_create(), image, boundingBoxes[1])
            ok = tracker.add(cv2.TrackerMIL_create(), image, boundingBoxes[2])
            ok = tracker.add(cv2.TrackerMIL_create(), image, boundingBoxes[3])
            init_once = True

        ok, boxes = tracker.update(image)

        # tracked_windows=list(map(getTrackedWindows,zip(boundingBoxes,roi_hists)))
        # del boundingBoxes[:]
        # boundingBoxes=tracked_windows

        for newbox in boxes:
            p1 = (int(newbox[0]), int(newbox[1]))
            p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
            cv2.rectangle(image, p1, p2, (200, 0, 0), 2)

        cv2.imshow('tracking', image)
        k = cv2.waitKey(1)
        if k == 27: break  # esc pressed
    # plt.show()
    # plt.scatter(*zip(*centres))
    # plt.show()
    cv2.destroyAllWindows()