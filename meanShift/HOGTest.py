import numpy as np
import cv2
import imutils



objectInTracking=[]

FLANN_INDEX_KDTREE = 0

index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks = 5000)
flann = cv2.FlannBasedMatcher(index_params, search_params)

class HumanObject:
    # found=0,0,0,0
    # area=0
    # siftKey,siftDes='',''
    # subImage=''
    # frame=''
    # tracker =''
    tracker = cv2.MultiTracker_create()

    objId=1

    def __init__(self,found,frame):
        self.found=found
        self.frame=frame
        self.area=self.getArea()
        self.subImage=self.getSubsetOfFrame()
        self.siftKey,self.siftDes=self.getSiftKeyDes()
        # self.tracker=cv2.TrackerKCF_create()
        x,y,w,h=self.found
        HumanObject.tracker.add(cv2.TrackerKCF_create(),frame,(x,y,w,h))
        # self.tracker.init(frame,(x,y,w,h))
        self.objId=''
        self.weight=''



    def getId(self):
        return self.objId

    def getWeight(self):
        return self.weight

    def setWeight(self,weight):
        self.weight=weight

    def setId(self,objId):
        self.objId=objId

    def track(self,frame):
        ok, bbox = self.tracker.update(frame)
        return ok,bbox

    def trackerinit(self,img,bbox):
        self.tracker.init(frame, bbox)

    def getArea(self):
        x,y,w,h=self.found
        return w*h

    def getSubsetOfFrame(self):
        x,y,w,h=self.found
        subImage=self.frame[y:y+h,x:x+w]
        return subImage

    def getSiftKeyDes(self):
        detector = cv2.xfeatures2d.SIFT_create()
        keyT, desT = detector.detectAndCompute(self.subImage, None)
        return keyT,desT

    def getFound(self):
        return self.found

    def setFound(self,found):
        self.found=found


def getROI(r,c,w,h,frame):
    roi = frame[c:c + h, r:r + w]
    hsv_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv_roi, np.array((0., 60., 32.)), np.array((180., 255., 255.)))
    roi_hist = cv2.calcHist([hsv_roi], [0], mask, [180], [0, 180])
    cv2.normalize(roi_hist, roi_hist, 0, 255, cv2.NORM_MINMAX)
    return roi_hist


def inside(r, q):
    rx, ry, rw, rh = r
    qx, qy, qw, qh = q
    return rx > qx and ry > qy and rx + rw < qx + qw and ry + rh < qy + qh


def updateTracking(img,objects):
    term_crit = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)
    for object in objects:
        x,y,w,h=object.getFound()
        # the HOG detector returns slightly larger rectangles than the real objects.
        # so we slightly shrink the rectangles to get a nicer output.
        roi_hist=getROI(x,y,w,h,img)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        dst = cv2.calcBackProject([hsv], [0], roi_hist, [0, 180], 1)
        track_window=(x,y,w,h)
        # apply meanshift to get the new location
        ret, track_window = cv2.meanShift(dst, track_window, term_crit)
        draw_detections(img,[track_window])
        object.setFound(track_window)


def track(img,objectTracking):
    updateTracking(img,objectTracking)



def draw_detections_test(img, rects, thickness = 1):
    for x,y,w,h in rects:
        pad_w, pad_h = int(0.15*w), int(0.05*h)
        cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0, 255, 0), thickness)

def filterRegions(founds,w):
    areas=getAreaList([],founds)
    acceptedRegionInx=getAccIdx([],areas)
    numOfTuples=founds.shape[0]
    maskList=[False]*numOfTuples
    for x in acceptedRegionInx:
        maskList[x]=True
    founds=[d for d, s in zip(founds, maskList) if s]
    weights = [d for d, s in zip(w, maskList) if s]
    return founds,weights


def getAreaList(areas,founds):
    for x,y,w,h in founds:
        area=getArea(x,y,w,h)
        areas.append(area)
    return areas

def getArea(x,y,w,h):
    return w*h


def getAccIdx(Idx,areas):
    max = 0
    for x in areas:
        if x > max:
            max = x
    areasNormalize = [x / max for x in areas]
    mean = np.mean(areasNormalize)
    areasMinusMean = [abs(x - mean) for x in areasNormalize]
    Idx = [i for i, x in enumerate(areasMinusMean) if x < .20]
    return Idx


def updateTrackedObjectList(oldObjects,newObjects):
    FLANN_INDEX_KDTREE = 0

    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=5000)
    flann = cv2.FlannBasedMatcher(index_params, search_params)

    for newObject in newObjects:
        keyNew,desNew = newObject.getSiftKeyDes()
        for oldObject in oldObjects:
            keyOld,desOld=oldObject.getSiftKeyDes()
            matches = flann.knnMatch(desNew, desOld, k=2)
            good_matches=[]
            for i, (m, n) in enumerate(matches):
                if m.distance < 0.7 * n.distance:
                    hello=[]


def checkIfAlreadyExist(found,frame):
    #convert new founding into an object
    newObject=HumanObject(found,frame)
    #get sift key and des for new founding
    newObjKey,newObjDes=newObject.getSiftKeyDes()
    #compare new founding's sift points to every object in track to decide whether newly fouded region is distinct or not
    for object in objectInTracking:
        objKey,objDes=object.getSiftKeyDes()
        #compare area to decide wich should be the query image and which should be the test (test>query)
        if object.getArea()>=newObject.getArea():
            matches = flann.knnMatch(newObjDes, objDes, k=2)
        else:
            matches = flann.knnMatch(objDes, newObjDes, k=2)
        #calculate num of good matches using ratio test
        goodMatches=0
        for (m,n) in matches:
            if m.distance < 0.7*n.distance:
                goodMatches=goodMatches+1
        #use threshold of 10 to see if regions are same of not
        if goodMatches>1000:
            #if same then return true otherwise return false
            object.setFound(newObject.getFound())
            return True
    return False




def draw_detections(img, rects,w, thickness = 2):
    for (x,y,w,h),weight in zip(rects,w):
        pad_w, pad_h = int(0.15*w), int(0.05*h)
        if(weight>.5 and y+pad_h>300 and (y+h-pad_h)<700):
            cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0, 0, 255), thickness)





if __name__ == '__main__':

    hog = cv2.HOGDescriptor()
    hog.setSVMDetector( cv2.HOGDescriptor_getDefaultPeopleDetector() )
    cap=cv2.VideoCapture('Media/Video/QueueNew.mp4')


    while True:
        _, frame = cap.read()
        frame = imutils.resize(frame, width=min(1600, frame.shape[1]))
        found, w = hog.detectMultiScale(frame, winStride=(8, 8), padding=(32, 32), scale=1.05)
        if (found != ()):
            draw_detections(frame,found,w)
            cv2.imshow('feed',frame)
            ch = 0xFF & cv2.waitKey(1)
            if ch == 27:
                break
    cv2.destroyAllWindows()