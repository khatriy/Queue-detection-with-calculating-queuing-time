import numpy as np
import cv2 
import imutils
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
cap = cv2.VideoCapture('Media/Video/QueueNew.mp4')
# take first frame of the video
ret,frame = cap.read()
# setup initial location of window
r,h,c,w = 250,90,400,125  # simply hardcoded the values



def getRoi(frame,found):
    x,y,w,h=found
    track_window = (x, y, w, h)
    # set up the ROI for tracking
    roi = frame[y:y + h, x:x + w]
    hsv_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv_roi, np.array((0., 60., 32.)), np.array((180., 255., 255.)))
    roi_hist = cv2.calcHist([hsv_roi], [0], mask, [180], [0, 180])
    cv2.normalize(roi_hist, roi_hist, 0, 255, cv2.NORM_MINMAX)
    return roi_hist
    # Setup the termination criteria, either 10 iteration or move by atleast 1 pt

term_crit = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1)

ret ,frame = cap.read()
frame = imutils.resize(frame, width=min(800, frame.shape[1]))
# founds, w = hog.detectMultiScale(frame, winStride=(8, 8), padding=(32, 32), scale=1.05)

r = cv2.selectROI(frame)
founds=[]
founds.append(r)
while(1):
    ret ,frame = cap.read()
    frame = imutils.resize(frame, width=min(800, frame.shape[1]))

    for found in founds:
        if ret == True:
            x,y,w,h=found
            track_window=(x,y,w,h)
            hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
            roi_hist=getRoi(frame,found)
            dst = cv2.calcBackProject([hsv],[0],roi_hist,[0,180],1)
            # apply meanshift to get the new location
            ret, track_window = cv2.meanShift(dst, track_window, term_crit)
            # Draw it on image
            x,y,w,h = track_window
            img2 = cv2.rectangle(frame, (x,y), (x+w,y+h), 255,2)
            founds=[]
            founds.append(track_window)
            cv2.imshow('img2',img2)
            k = cv2.waitKey(60) & 0xff
            if k == 27:
                break
            else:
                cv2.imwrite(chr(k)+".jpg",img2)
        else:
            break
cv2.destroyAllWindows()
cap.release()