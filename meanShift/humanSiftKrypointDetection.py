import cv2
import numpy as np
import logging

logger = logging.getLogger('spam_application')
logger.setLevel(logging.DEBUG)


def getGoodMatches(desFrame1,desFrame2):

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=5000)
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(desFrame1, desFrame2, k=2)

    matchesMask = [[0, 0] for i in range(len(matches))]

    return matches,matchesMask

def getDiffKeypoints(keyFrame1,keyFrame2,matches,matchMask):
    intersect=[]
    good=[]
    k=0
    for i,(m, n) in enumerate(matches):
        if m.distance < 0.7 * n.distance:
            srcPoint = keyFrame1[m.queryIdx].pt
            dstPoint = keyFrame2[m.trainIdx].pt
            srcX = srcPoint[0]
            srcY = srcPoint[1]
            dstX = dstPoint[0]
            dstY = dstPoint[1]
            if (srcX >= dstX - 15.0 and srcX <= dstX + 15.0) and (srcY >= dstY - 15.0 and srcY <= dstY + 15.0):
                logger.info('srcPoint - ' + str(srcPoint) + ', Dst Point - ' + str(dstPoint) + ', Equal')
                intersect.append(True)
            else:
                logger.info('srcPoint - ' + str(srcPoint) + ', Dst Point - ' + str(dstPoint) + ', Not Equal')
                intersect.append(False)
                matchMask[i] = [1, 0]
                good.append(m)

                k = k + 1
        logger.info('value of k is ' + str(k))
    return good,matchMask



    # src_pts = np.float32([keyFrame1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    # dst_pts = np.float32([keyFrame2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    # h,w,d=src_pts.shape
    # intersect=[]
    # k = 0
    #
    # for i in range(h):
    #     srcPoint=src_pts[i][0]
    #     dstPoint=dst_pts[i][0]
    #     srcX=srcPoint[0]
    #     srcY=srcPoint[1]
    #     dstX=dstPoint[0]
    #     dstY=dstPoint[1]
    #     if (srcX>=dstX-100.0 and srcX<=dstX+100.0) and (srcY>=dstY-100.0 and srcY<=dstY+100.0):
    #         logger.info('srcPoint - '+str(srcPoint)+', Dst Point - '+str(dstPoint)+', Equal')
    #         intersect.append(True)
    #         matchMask[i] = [0, 0]
    #     else:
    #         logger.info('srcPoint - '+str(srcPoint)+', Dst Point - '+str(dstPoint)+', Not Equal')
    #         intersect.append(False)
    #         k=k+1
    # logger.info('value of k is '+str(k))

#read video frame by frame
logger.info('reading video file')
cap = cv2.VideoCapture('Media/Video/SiftDetector.mp4')
logger.info('creating sift')
detector = cv2.xfeatures2d.SIFT_create()

logger.info('readubg first frame')
ret,frameOld=cap.read()
logger.info('compute keypoints and des of first frame')
keyFrameOld, desFrameOld = detector.detectAndCompute(frameOld, None)

logger.info('while loop start')
i=0
while(1):
    logger.info('read new frame and calculate keypoints and descriptors')
    ret,frameNew = cap.read()
    keyFrameNew, desFrameNew = detector.detectAndCompute(frameNew, None)
    logger.info('calculating matches')
    matches, matchesMask = getGoodMatches(desFrameOld, desFrameNew)
    good, matchesMask = getDiffKeypoints(keyFrameOld, keyFrameNew, matches, matchesMask)
    logger.info('total good macthes are {:d}'.format(len(good)))
    src_pts = np.float32([keyFrameOld[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([keyFrameNew[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    logger.info('calculating homography')
    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    matchesMask1 = mask.ravel().tolist()
    draw_params_copy = dict(matchColor=(0, 255, 0),  # draw matches in green color
                            singlePointColor=None,
                            matchesMask=matchesMask1,  # draw only inliers
                            flags=2)
    logger.info('drawing matches')
    img2 = cv2.drawMatches(frameOld, keyFrameOld, frameNew, keyFrameOld, good, None, **draw_params_copy)
    cv2.imshow('imageCopy{:d}'.format(i), cv2.resize(img2, (0, 0), fx=0.5, fy=0.5))
    i=i+1
    logger.info('swap old frame values with new one and recaluclate new one')
    frameOld=frameNew
    keyFrameOld=keyFrameNew
    desFrameOld=desFrameNew
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()
