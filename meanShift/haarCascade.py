import numpy as np
import cv2
import imutils




face_cascade = cv2.CascadeClassifier('/Users/kere-mac/anaconda3/lib/python3.6/site-packages/cv2/data/haarcascade_frontalface_default.xml')






cap=cv2.VideoCapture('Media/Video/digiHomeFacial.mp4')

while True:
    _, frame = cap.read()
    frame = imutils.resize(frame, width=min(800, frame.shape[1]))
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    option=0
    for (x, y, w, h) in faces:
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        font = cv2.FONT_HERSHEY_SIMPLEX
        if(option==0):
            name='Nial'
            option=1
        else:
            name='Kevin'
        cv2.putText(frame, name, (x, y), font, 1, (200, 255, 155))

    # find human region
    # compare if diff from previous one
    # if diff then update previous one
    # if not then just track detected region using mean shift or cam shift

    # faces = face_cascade.detectMultiScale(cv2.cvtColor(frame,cv2.COLOR_RGB2GRAY), 1.1, 1)
    cv2.imshow('feed', frame)
    ch = 0xFF & cv2.waitKey(1)
    if ch == 27:
        break
cv2.destroyAllWindows()
