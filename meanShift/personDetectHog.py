import cv2

image=cv2.imread('Media/Pictures/BOKPeople.jpg')
winStride = (4,4)
padding = (16,16)
meanShift = True
scale=1.01
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
(rects, weights) = hog.detectMultiScale(image, winStride=winStride,
                                        padding=padding, scale=scale, useMeanshiftGrouping=meanShift)
for (x, y, w, h) in rects:
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
imr=cv2.resize(image, (960, 540))
cv2.imshow("finalImage",imr)
cv2.waitKey(0)